from django.shortcuts import render
from django.urls import reverse_lazy

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import Contribuyente, Aporte, Cundina, Gerente
from .forms import NewContribuyenteForm, NewAporteForm, CundinaForm, GerenteForm

# Create your views here.



##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListContribuyente(LoginRequiredMixin, generic.ListView):
    template_name = "onv/list_contribuyente.html"
    model = Contribuyente
    context_object_name = "obj"
    login_url = "home:login"

## generic.DetailView
class DetailContribuyente(LoginRequiredMixin, generic.DetailView):
    template_name = "onv/detail_contribuyente.html"
    model = Contribuyente
    login_url = "home:login"
    context_object_name = "obj"



##### CREATE #####
## generic.CreateView
class NewContribuyente(LoginRequiredMixin, generic.CreateView):
    template_name = "onv/new_contribuyente.html"
    model = Contribuyente
    context_object_name = "obj"
    form_class = NewContribuyenteForm
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_contribuyente")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateContribuyente(LoginRequiredMixin, generic.UpdateView):
    template_name = "onv/update_contribuyente.html"
    model = Contribuyente
    context_object_name = "obj"
    form_class = NewContribuyenteForm
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_contribuyente")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteContribuyente(LoginRequiredMixin, generic.DeleteView):
    template_name = "onv/delete_contribuyente.html"
    model = Contribuyente
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_contribuyente")

class ListAporte(LoginRequiredMixin, generic.ListView):
    template_name = "onv/list_aporte.html"
    model = Aporte
    context_object_name = "obj"
    login_url = "home:login"


class DetailAporte(LoginRequiredMixin, generic.DetailView):
    template_name = "onv/detail_aporte.html"
    model = Aporte
    login_url = "home:login"
    context_object_name = "obj"


##### CREATE #####
## generic.CreateView
class NewAporte(LoginRequiredMixin, generic.CreateView):
    template_name = "onv/new_aporte.html"
    model = Aporte
    context_object_name = "obj"
    form_class = NewAporteForm
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_aporte")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


##### UPDATE #####
## generic.UpdateView
class UpdateAporte(LoginRequiredMixin, generic.UpdateView):
    template_name = "onv/update_aporte.html"
    model = Aporte
    context_object_name = "obj"
    form_class = NewAporteForm
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_aporte")


    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### DELETE #####
## generic.DeleteView
class DeleteAporte(LoginRequiredMixin, generic.DeleteView):
    template_name = "onv/delete_aporte.html"
    model = Aporte
    context_object_name = "obj"
    success_url = reverse_lazy("onv:list_aporte")
    login_url = "home:login"


##### API's #####
## ws JSON

def wsList(request):
    data = serializers.serialize('json', Category.objects.filter(status=False))
    return HttpResponse(data, content_type="application/json")

class ListGerente(LoginRequiredMixin, generic.ListView):
    template_name = "onv/list_gerente.html"
    model = Gerente
    context_object_name = "obj"
    login_url = "home:login"


class NewGerente(LoginRequiredMixin, generic.CreateView):
    template_name = "onv/new_gerente.html"
    model = Gerente
    form_class = GerenteForm
    login_url = "home:login"
    context_object_name = "obj"
    success_url = reverse_lazy("onv:list_gerente")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class UpdateGerente(LoginRequiredMixin, generic.UpdateView):
    template_name = "onv/update_gerente.html"
    model = Gerente
    form_class = GerenteForm
    login_url = "home:login"
    context_object_name = "obj"
    success_url = reverse_lazy("onv:list_gerente")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class DeleteGerente(LoginRequiredMixin, generic.DeleteView):
    template_name = "onv/delete_gerente.html"
    model = Gerente
    context_object_name = "obj"
    success_url = reverse_lazy("onv:list_gerente")
    login_url = "home:login"



##### CRUD Measure #####


##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListCundina(LoginRequiredMixin, generic.ListView):
    template_name = "onv/list_cundina.html"
    model = Cundina
    context_object_name = "obj"
    login_url = "home:login"

## generic.DetailView
# class DetailCategory(LoginRequiredMixin, generic.DetailView):
#     template_name = "inv/detail_category.html"
#     model = Category
#     login_url = "home:login"
#     context_object_name = "obj"



##### CREATE #####
## generic.CreateView
class NewCundina(LoginRequiredMixin, generic.CreateView):
    template_name = "onv/new_cundina.html"
    model = Cundina
    context_object_name = "obj"
    form_class = CundinaForm
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_cundina")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateCundina(LoginRequiredMixin, generic.UpdateView):
    template_name = "onv/update_cundina.html"
    model = Cundina
    context_object_name = "obj"
    form_class = CundinaForm
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_cundina")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteCundina(LoginRequiredMixin, generic.DeleteView):
    template_name = "onv/delete_cundina.html"
    model = Cundina
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_cundina")
