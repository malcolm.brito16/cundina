from django.urls import path

from onv import views

app_name = "onv"

urlpatterns = [
    path('list_contribuyente/', views.ListContribuyente.as_view(), name="list_contribuyente"),
    path('new_contribuyente/', views.NewContribuyente.as_view(), name="new_contribuyente"),
    path('update_contribuyente/<int:pk>/', views.UpdateContribuyente.as_view(), name="update_contribuyente"),
    path('delete_contribuyente/<int:pk>/', views.DeleteContribuyente.as_view(), name="delete_contribuyente"),
    path('new_aporte/', views.NewAporte.as_view(), name="new_aporte"),
    path('list_aporte/', views.ListAporte.as_view(), name="list_aporte"),
    path('update_aporte/<int:pk>/', views.UpdateAporte.as_view(), name="update_aporte"),
    path('delete_aporte/<int:pk>/', views.DeleteAporte.as_view(), name="delete_aporte"),
    path('detail_contribuyente/<int:pk>/', views.DetailContribuyente.as_view(), name="detail_contribuyente"),
    path('detail_aporte/<int:pk>/', views.DetailAporte.as_view(), name="detail_aporte"),
    path('ws/list_contribuyente/', views.wsList, name="ws_list_contribuyente"),
    path('list_gerente/', views.ListGerente.as_view(), name="list_gerente"),
    path('new_gerente/', views.NewGerente.as_view(), name="new_gerente"),
    path('update_gerente/<int:pk>/', views.UpdateGerente.as_view(), name="update_gerente"),
    path('delete_gerente/<int:pk>/', views.DeleteGerente.as_view(), name="delete_gerente"),
    path('list_cundina/', views.ListCundina.as_view(), name="list_cundina"),
    path('new_cundina/', views.NewCundina.as_view(), name="new_cundina"),
    path('update_cundina/<int:pk>/', views.UpdateCundina.as_view(), name="update_cundina"),
    path('delete_cundina/<int:pk>/', views.DeleteCundina.as_view(), name="delete_cundina"),

    ]
