from django import forms

from .models import Contribuyente, Aporte, Gerente, Cundina

class NewContribuyenteForm(forms.ModelForm):
    class Meta:
        model = Contribuyente
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Category",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
class NewAporteForm(forms.ModelForm):
    class Meta:
        model = Aporte
        fields = [
            "contribuyente",
            "description",
            "status",
        ]
        labels = {
            "description": "Contribuyente",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
            self.fields["contribuyente"].empty_label = "Choose Contribuyentes"

class GerenteForm(forms.ModelForm):
    class Meta:
        model = Gerente
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Brand",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })


class CundinaForm(forms.ModelForm):
    class Meta:
        model = Cundina
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Cundina",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
