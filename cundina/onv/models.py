from django.db import models
from django.contrib.auth.models import User



# Create your models here.

class GerenteQuerySet(models.QuerySet):
    def old(self):
        return self.filter(id__lt=3)


class BaseModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    user_update = models.IntegerField(blank=True, null=True)


    class Meta:
        abstract = True


class Contribuyente(BaseModel):
    description = models.CharField(
        max_length = 128,
        help_text = "Contribuyente Description",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Contribuyente, self).save()

    class Meta:
        verbose_name_plural = "Contribuyentes"


class Aporte(BaseModel):
    contribuyente = models.ForeignKey(Contribuyente, on_delete=models.CASCADE)
    description = models.CharField(
        max_length = 128,
        help_text = "Aporte Description"
    )

    def __str__(self):
        return "{0} - {1}".format(self.contribuyente.description, self.description)


    def save(self):
        self.description = self.description.upper()
        super(Aporte, self).save()

    class Meta:
        verbose_name_plural = "Aportes"
        unique_together = ("contribuyente", "description")

class Gerente(BaseModel):
    objects = GerenteQuerySet.as_manager()
    description = models.CharField(
        max_length = 128,
        help_text = "Description Gerente",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Gerente, self).save()

    class Meta:
        verbose_name_plural = "Gerentes"



class Cundina(BaseModel):
    description = models.CharField(
        max_length = 128,
        help_text = "Cundina Description",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Cundina, self).save()

    class Meta:
        verbose_name_plural = "Cundinas"
