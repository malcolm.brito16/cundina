from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Turnos,Contribucion
from .forms import TurnosForm, ContribucionForm
# Create your views here.

class ListTurnos(LoginRequiredMixin, generic.ListView):
    template_name = "shi/list_turnos.html"
    model = Turnos
    context_object_name = "obj"
    login_url = "home:login"

##### CREATE #####
## generic.CreateView
class NewTurnos(LoginRequiredMixin, generic.CreateView):
    template_name = "shi/new_turnos.html"
    model = Turnos
    context_object_name = "obj"
    form_class = TurnosForm
    login_url = "home:login"
    success_url = reverse_lazy("shi:list_turnos")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateTurnos(LoginRequiredMixin, generic.UpdateView):
    template_name = "shi/update_turnos.html"
    model = Turnos
    context_object_name = "obj"
    form_class = TurnosForm
    login_url = "home:login"
    success_url = reverse_lazy("onv:list_turnos")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteTurnos(LoginRequiredMixin, generic.DeleteView):
    template_name = "shi/delete_turnos.html"
    model = Turnos
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("shi:list_Turnos")

class ListContribucion(LoginRequiredMixin, generic.ListView):
    template_name = "shi/list_contribucion.html"
    model = Contribucion
    context_object_name = "obj"
    login_url = "home:login"


##### CREATE #####
## generic.CreateView
class NewContribucion(LoginRequiredMixin, generic.CreateView):
    template_name = "shi/new_contribucion.html"
    model = Contribucion
    context_object_name = "obj"
    form_class = ContribucionForm
    login_url = "home:login"
    success_url = reverse_lazy("shi:list_contribucion")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateContribucion(LoginRequiredMixin, generic.UpdateView):
    template_name = "shi/update_category.html"
    model = Contribucion
    context_object_name = "obj"
    form_class = ContribucionForm
    login_url = "home:login"
    success_url = reverse_lazy("shi:list_contribucion")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteContribucion(LoginRequiredMixin, generic.DeleteView):
    template_name = "shi/delete_contribucion.html"
    model = Contribucion
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("shi:list_contribucion")
