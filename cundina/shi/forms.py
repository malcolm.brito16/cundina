from django import forms

from .models import Contribucion, Turnos

class ContribucionForm(forms.ModelForm):
    class Meta:
        model = Contribucion
        fields = [ ## "__all__"
            "cundina",
            "contribuyente",
            "aporte",
        ]
        exclude = ["user", "user_update", "timestamp", "update"]
        labels = {
            "cundina": "Cundina",
            "contribuyente": "Contribuyente",
            "aporte": "Aporte",
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })

class TurnosForm(forms.ModelForm):
    class Meta:
        model = Turnos
        fields = [ ## "__all__"
            "cundina",
            "turnos",
            "gerente",
        ]
        exclude = ["user", "user_update", "timestamp", "update"]
        labels = {
            "cundina": "Cundina",
            "turnos": "Turnos",
            "gerente": "Gerente",
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
