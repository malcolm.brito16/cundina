from django.urls import path

from shi import views

app_name = "shi"

urlpatterns =[
    path('list_turnos/', views.ListTurnos.as_view(), name="list_turnos"),
    path('new_turnos/', views.NewTurnos.as_view(), name="new_turnos"),
    path('update_turnos/<int:pk>/', views.UpdateTurnos.as_view(), name="update_turnos"),
    path('delete_turnos/<int:pk>/', views.DeleteTurnos.as_view(), name="delete_turnos"),
    path('new_contribucion/', views.NewContribucion.as_view(), name="new_contribucion"),
    path('list_contribucion/', views.ListContribucion.as_view(), name="list_contribucion"),
    path('update_contribucion/<int:pk>/', views.UpdateContribucion.as_view(), name="update_contribucion"),
    path('delete_contribucion/<int:pk>/', views.DeleteContribucion.as_view(), name="delete_contribucion"),
]
