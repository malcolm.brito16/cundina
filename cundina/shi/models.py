from django.db import models

from onv.models import BaseModel, Gerente, Contribuyente, Aporte, Cundina

# Create your models here.
class Turnos(BaseModel):
    cundina = models.ForeignKey(Cundina, on_delete=models.CASCADE)
    turnos = models.FloatField(default=0)
    gerente = models.ForeignKey(Gerente, on_delete=models.CASCADE)


    def save(self):
        super(Turnos, self).save()


    class Meta:
        verbose_name_plural = "Turnos"


class Contribucion(BaseModel):
    cundina = models.ForeignKey(Cundina, on_delete=models.CASCADE)
    contribuyente = models.ForeignKey(Contribuyente, on_delete=models.CASCADE)
    aporte = models.ForeignKey(Aporte, on_delete=models.CASCADE)


    def save(self):
        super(Contribucion, self).save()


    class Meta:
        verbose_name_plural = "Turnos"
